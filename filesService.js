let fs = require('fs');
let path = require('path');

const dir = '/files'
const dirFiles = path.join(__dirname, dir)
const exetNames = ['.log', '.txt', '.json', '.yaml', '.xml', '.js']


function createFile(req, res, next) {
  let checkExten = false;
  const filename = req.body.filename
  if (!req.body.hasOwnProperty('filename') || filename.length === 0) {
    res.status(400).send({ "message": "Please specify 'filename' parameter" });
    return
  } else if (!req.body.hasOwnProperty('content')) {
    res.status(400).send({ "message": "Please specify 'content' parameter" });
    return
  }

  exetNames.forEach(item => {
    if (filename.slice(filename.lastIndexOf('.')) === item) {
      checkExten = true
    }
  })

  if (!checkExten) {
    res.status(400).send({ "message": "File should have ['.log', '.txt', '.json', '.yaml', '.xml', '.js'] extensions" });
    return
  }

  fs.writeFile(path.join(dirFiles, filename), JSON.stringify(req.body), (err) => {
    if (err) {
      res.status(500).send({ "message": "Server error" });
    } else {
      res.status(200).send({ "message": "File created successfully" });
    }
  });
}

function getFiles(req, res, next) {
  fs.readdir(dirFiles, (err, files) => {
    if (err) {
      res.status(500).send({ "message": "Server error" });
    } else {
      res.status(200).send({
        "message": "Success",
        "files": files
      });
    }
  });
}

const getFile = (req, res, next) => {
  const file = path.join(dirFiles, req.url)
  if (!fs.existsSync(file)) {
    res.status(400).send({ "message": `No file with ${req.url} filename found` });
    return
  }
  const { birthtime } = fs.statSync(file);
  fs.readFile(file, 'utf8', (err, data) => {
    if (err) {
      res.status(500).send({ "message": "Server error" });
    } else {
      res.status(200).send({
        "message": "Success",
        "filename": path.basename(file),
        "content": JSON.parse(data).content,
        "extension": path.extname(file).slice(1),
        "uploadedDate": birthtime
      });
    }
  });
}

const editFile = (req, res, next) => {
  if (!req.body.hasOwnProperty('filename') || req.body.filename.length === 0) {
    res.status(400).send({ "message": "Please specify 'filename' parameter" });
    return
  } else if (!req.body.hasOwnProperty('content')) {
    res.status(400).send({ "message": "Please specify 'content' parameter" });
    return
  }

  const file = path.join(dirFiles, req.body.filename)
  if (!fs.existsSync(file)) {
    res.status(400).send({ "message": `No file with ${req.body.filename} filename found` });
    return
  }

  fs.writeFile(path.join(dirFiles, req.body.filename), JSON.stringify(req.body), (err) => {
    if (err) {
      res.status(500).send({ "message": "Server error" });
    } else {
      res.status(200).send({ "message": "File edited successfully" });
    }
  });
}

const deleteFile = (req, res, next) => {
  const file = path.join(dirFiles, req.url)
  if (!fs.existsSync(file)) {
    res.status(400).send({ "message": `No file with ${req.url} filename found` });
    return
  }
  fs.unlink(path.join(dirFiles, req.url), err => {
    if (err) {
      res.status(500).send({ "message": "Server error" });
    } else {
      res.status(200).send({ "message": `File ${req.url} deleted successfully` });
    }
  });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
