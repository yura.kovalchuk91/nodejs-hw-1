const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express();

const { filesRouter } = require('./filesRouter.js');

let logStream = fs.createWriteStream(path.join(__dirname, 'logger.log'), { flags: 'a' })

app.use(morgan('combined', { stream: logStream }))

// morgan.token('filename', req => JSON.stringify(req.body.filename))
// morgan.token('content', req => JSON.stringify(req.body.content))
// app.use(morgan('Created file - :filename [:date[clf]]\nWith content :content'));
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler(err, req, res, next) {
  console.error('err')
  res.status(500).send({ 'message': 'Server error' });
}
